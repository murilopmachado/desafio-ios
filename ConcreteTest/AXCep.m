//
//  AXCep.m
//  SDKTests
//
//  Created by Alexandre Garrefa on 9/26/14.
//  Copyright (c) 2014 Alexandre. All rights reserved.
//

#import "AXCep.h"

@implementation AXCep

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"cep"       : @"cep",
             @"tipo"      : @"tipoDeLogradouro",
             @"logradouro": @"logradouro",
             @"bairro"    : @"bairro",
             @"cidade"    : @"cidade",
             @"estado"    : @"estado"};
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError **)error
{
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self == nil) return nil;
    
    _searchDate = [NSDate date];
    [self persist];
    
    return self;
}

- (instancetype)initWithJsonDictionary:(NSDictionary *)json error:(NSError **)error
{
    return [MTLJSONAdapter modelOfClass:self.class fromJSONDictionary:json error:error];
}

#pragma mark -
#pragma mark - Private Methods

+ (AXCep *)fetchCep:(NSString *)cep
{
    AXCep *item = nil;
    NSString *path = [self storagePathForCep:cep];
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        NSData *data = [NSData dataWithContentsOfFile:path];
        NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
        item = [[[self class] alloc] initWithCoder:unarchiver];
        [unarchiver finishDecoding];
    }
    return item;
}

- (void)persist
{
    NSMutableData *data = [NSMutableData data];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [self encodeWithCoder:archiver];
    [archiver finishEncoding];
    [data writeToFile:[[self class] storagePathForCep:self.cep] atomically:YES];
}


+ (NSString *)storagePath
{
    NSString *cachesDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    NSString *storagePath = [cachesDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"br.com.concretetest.data/%@", NSStringFromClass([self class])]];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:storagePath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:storagePath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    return storagePath;
}

+ (NSString *)storagePathForCep:(NSString *)cep
{
    return [NSString stringWithFormat:@"%@/%@.data", self.storagePath, cep];
}

+ (NSArray *)savedObjects
{
    NSArray *directoryContent =
    [[NSFileManager defaultManager] contentsOfDirectoryAtPath:self.storagePath error:NULL];
    NSMutableArray *aux = [[NSMutableArray alloc] initWithCapacity:directoryContent.count];
    for (NSString *file in directoryContent) {
        NSString *cep = [file stringByReplacingOccurrencesOfString:@".data" withString:@""];
        [aux addObject:cep];
    }
    return aux;
}

+ (NSError *)removeAllSavedObjects
{
    NSError *error = nil;
    
    [[NSFileManager defaultManager] removeItemAtPath:self.storagePath error:&error];
    
    return error;
}

#pragma mark -
#pragma mark - Utils

- (NSAttributedString *)attributedDescription
{
    NSMutableString *cep = [NSMutableString stringWithString:self.cep];
    [cep insertString:@"-" atIndex:5];
    
    NSString *line1 = [NSString stringWithFormat:@"%@\n",cep];
    NSString *line2 = [NSString stringWithFormat:@"%@ ",self.tipo];
    NSString *line3 = [NSString stringWithFormat:@"%@\n",self.logradouro];
    NSString *line4 = [NSString stringWithFormat:@"%@\n",self.bairro];
    NSString *line5 = [NSString stringWithFormat:@"%@ - ",self.cidade];
    NSString *line6 = [NSString stringWithFormat:@"%@",self.estado];
    NSString *text = [NSString stringWithFormat:@"%@%@%@%@%@%@",line1,line2,line3,line4,line5,line6];
    
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.lineSpacing = 2.0f;
    
    NSMutableAttributedString *description = [[NSMutableAttributedString alloc] initWithString:text];
    [description addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15.0f] range:NSMakeRange(0, text.length)];
    [description addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:NSMakeRange(0, text.length)];
    [description addAttribute:NSParagraphStyleAttributeName value:paragraph range:NSMakeRange(0, text.length)];
    
    return description;
}

@end