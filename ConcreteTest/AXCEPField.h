//
//  AXCEPField.h
//  SDKTests
//
//  Created by Alexandre Garrefa on 9/29/14.
//  Copyright (c) 2014 Alexandre. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AXCEPFieldDelegate <NSObject>
@optional
-(void)ax_cepFieldDidBeginEditing:(UITextField *)textField;
@end

@interface AXCEPField : UITextField
@property (nonatomic, strong) NSObject <AXCEPFieldDelegate> *cepDelegate;
@end
