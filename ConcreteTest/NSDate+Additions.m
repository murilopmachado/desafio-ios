//
//  NSDate+Additions.m
//  SDKTests
//
//  Created by Alexandre Garrefa on 10/1/14.
//  Copyright (c) 2014 Alexandre. All rights reserved.
//

#import "NSDate+Additions.h"

@implementation NSDate (Additions)

+ (NSDateFormatter *)formatterWithFormat:(NSString *)format
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    return formatter;
}

- (NSString *)formatedWithFormat:(NSString *)format
{
    return [[NSDate formatterWithFormat:format] stringFromDate:self];
}

- (NSString *)shortFormat
{
    return [[NSDate formatterWithFormat:@"MM/dd/yyyy"] stringFromDate:self];
}

@end
