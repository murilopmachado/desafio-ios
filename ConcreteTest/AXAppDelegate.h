//
//  AXAppDelegate.h
//  ConcreteTest
//
//  Created by Alexandre Garrefa on 10/1/14.
//  Copyright (c) 2014 Alexandre. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AXAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
