//
//  AXCepSearch.h
//  SDKTests
//
//  Created by Alexandre Garrefa on 9/27/14.
//  Copyright (c) 2014 Alexandre. All rights reserved.
//

#import "AXAPIClient.h"

@class AXCep;
@interface AXCepSearch : AXAPIClient

+ (NSURLSessionDataTask *)searchForCep:(NSString *)cep withSuccessBlock:(void (^)(AXCep *cep, NSError *error))success
                       andFailureBlock:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

@end
