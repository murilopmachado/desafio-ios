
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Expecta
#define COCOAPODS_POD_AVAILABLE_Expecta
#define COCOAPODS_VERSION_MAJOR_Expecta 0
#define COCOAPODS_VERSION_MINOR_Expecta 2
#define COCOAPODS_VERSION_PATCH_Expecta 4

// Mantle
#define COCOAPODS_POD_AVAILABLE_Mantle
#define COCOAPODS_VERSION_MAJOR_Mantle 1
#define COCOAPODS_VERSION_MINOR_Mantle 5
#define COCOAPODS_VERSION_PATCH_Mantle 0

// Mantle/extobjc
#define COCOAPODS_POD_AVAILABLE_Mantle_extobjc
#define COCOAPODS_VERSION_MAJOR_Mantle_extobjc 1
#define COCOAPODS_VERSION_MINOR_Mantle_extobjc 5
#define COCOAPODS_VERSION_PATCH_Mantle_extobjc 0

// OCMock
#define COCOAPODS_POD_AVAILABLE_OCMock
#define COCOAPODS_VERSION_MAJOR_OCMock 2
#define COCOAPODS_VERSION_MINOR_OCMock 2
#define COCOAPODS_VERSION_PATCH_OCMock 4

// Specta
#define COCOAPODS_POD_AVAILABLE_Specta
#define COCOAPODS_VERSION_MAJOR_Specta 0
#define COCOAPODS_VERSION_MINOR_Specta 2
#define COCOAPODS_VERSION_PATCH_Specta 1

